<?php

namespace ncc_npp\user;

use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
public function boot()
{
    $this->loadRoutesFrom(__DIR__.'/routes.php');
    $this->loadViewsFrom(__DIR__.'views','user');
}

public function register()
{
//
}
}
